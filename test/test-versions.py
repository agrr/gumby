#!/usr/bin/env python
import sys
sys.path.append('./build/lib.linux-x86_64-2.6/')
import pygraphviz as pgv
import aptdottypy as apt

test_packages = ['bash', 'pidgin', 'glibc', 'firefox', 'timezone']

for p in test_packages:
    G = pgv.AGraph(directed='true')
    versions = apt.dotty_versions(p)
    if versions:
        for v in versions:
            G.add_edge(p, v)

        print G.string()            

    else:
        print 'Not available or Virtual Pkg'
