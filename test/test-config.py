import cPickle
import os
import pprint


def dump_config(filename):
    config = open(filename, 'rb')
    version = cPickle.load(config)
    prefs = cPickle.load(config)
    print 'Preferences: '
    pprint.pprint(prefs)
    config.close()

if __name__ == '__main__':
    dump_config(os.path.expanduser('~')+'/.local/share/gumby/preferences.pickle')

