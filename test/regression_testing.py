#!/usr/bin/env python
import sys
sys.path.append('build/lib.linux-x86_64-2.6/')

try:
    import aptdottypy
except ImportError:
    print 'Couldn\'t find module aptdottypy, maybe run python setup.py build first'
    sys.exit(-1)

for i in sys.argv[1:]:
    print 'Testing dotty_pkg(\'%s\')' % i
    g = aptdottypy.dotty_pkg(i)
    print len(g.edges)
    print len(g.edge_attrs)

'''
print 'Testing dotty_virtualpkg(\'curl-devel\')'
g1 = aptdottypy.dotty_virtualpkg('zsh')
print g1.edges
print g1.edge_attrs

print 'Testing dotty_pkg.dotty_versions'
print str(aptdottypy.dotty_versions('apt-pbo'))
print 'Testing dotty_pkg.dotty_match_package_names'
print str(aptdottypy.dotty_match_package_names('rpm'))
'''
