from ctypes import *
import sys



libc = CDLL("./aptpkg-gumby.so") 

args = sys.argv[1:] if len(sys.argv) > 1 else ["python-devel"]
what_provides = libc.what_provides
what_provides.restype = c_char_p
requires = libc.requires
requires.restype = c_char_p

for a in args:
   res = what_provides(a)
   print res

pkgs = ['nagios', 'bash', 'python', 'sqlite3-tools']

for p in pkgs:
    print "%s deps: %s" % (p, requires(p))
    print "\n"
    #print "Length: "+str(len(requires(p)))
