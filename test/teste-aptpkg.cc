/* Include Files							*{{{*/
//
#include <apt-pkg/error.h>
#include <apt-pkg/init.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/version.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <vector>
#include <set>
#include <stdlib.h>
#include <sstream>
#include <unistd.h>
#include <errno.h>

using namespace std;

static FILE* random_source;


//To build : g++ -O3 -lapt-pkg -o teste-aptpkg teste-aptpkg.cc


#define DLL_PUBLIC __attribute__ ((visibility("default")))
#define DLL_LOCAL  __attribute__ ((visibility("hidden")))


extern "C"
{
const char* what_provides(const char *vpkgname);
const char* requires(const char *name);
const char* pkg_search(const char *name);

};



static pkgCache* openCache()
{
   pkgCache *GCache;

   if (pkgInitConfig(*_config) == false ||
       pkgInitSystem(*_config,_system) == false)
   {
      _error->DumpErrors();
      return NULL;
   }

   MMap *Map = new MMap(*new FileFd(_config->FindFile("Dir::Cache::pkgcache"),
	    FileFd::ReadOnly),MMap::Public|MMap::ReadOnly);

   if (_error->PendingError() == false)
   {
      GCache = new pkgCache(Map);

   }
   else
   {
      return NULL;
   }

   return GCache;
}


#define IDSIZE 12
/*
 * Generate a pseudo-random fixed-size string to use as 
 * node id, or any other kind of unique ID 
 *
 */

static string randomString()
{
   
   char random_string[IDSIZE+1];
   unsigned int i=0;

   while(i!= IDSIZE)
   {
      sprintf(&random_string[i],"%x",getc(random_source));
      i++;
   }

   random_string[IDSIZE] = '\0';

   return string(random_string);
}


static bool IsSelfProvided(pkgCache * GCache, pkgCache::PkgIterator Pkg, 
      const char * Require)
{
     /* cerr << "Calling isSelfProvided for "<< Pkg.Name() << " and "<< Require; */
        for (pkgCache::PrvIterator I = Pkg.ProvidesList(); 
              I.end() == false; I++)
        {
	   if (strcmp(I.Name(), Require) == 0)
	    return true;
	   
	}

	return false;
}

// RequiresDotty - Generate a dot document representing the Requires of pkgName	/*{{{*/
// ---------------------------------------------------------------------
static string RequiresDotty(pkgCache *GCache, const char *pkgName)
{
   pkgCache &Cache = *GCache;
   
   string res;

   for (pkgCache::PkgIterator Pkg = Cache.PkgBegin(); Pkg.end() == false; Pkg++)
   {
      if (strcmp(Pkg.Name(), pkgName)!= 0)
		   continue;

      //TODO: We're arbitrarily taking the first version of Pkg,
      //we have to expand them all
      pkgCache::VerIterator Ver = Pkg.VersionList();
      for (pkgCache::DepIterator D = Ver.DependsList(); D.end() == false; D++)
      {
	 if (D.TargetPkg() == 0)
	    continue;
	 
   	 /*if (IsSelfProvided(GCache, Pkg, D.TargetPkg().Name()))
	    continue;
	 */

/*	 const int LABELSIZE=100;
	 char Label[LABELSIZE];
	 memset(Label, '\0', LABELSIZE); */

	 switch(D->Type)
     {
         case pkgCache::Dep::Depends:    
             res += "requires ";
             break;
         case pkgCache::Dep::Conflicts:
             res += "conflicts ";
             break;

         case pkgCache::Dep::Obsoletes:    
             res += "obsoletes ";
             break;
     }
	 const char* comptype = D.CompType() != 0 ? D.CompType(): NULL;
	 const char* version = D.TargetVer() != 0 ? D.TargetVer(): NULL;
	 if (strlen(comptype) > 0)
	 {
	    res += (string)comptype+" "; 
	    res += (string)version+" ";
	 }


	 res += (string)D.TargetPkg().Name();
	 res += "\n";

      }

   }

   return res;
}

/**
 * This returns a dot graph of virtual package expansions 
 */

string WhatProvidesDotty(pkgCache *GCache, const char *virtualPkgName)
{

   bool found;
   ostringstream out;
   pkgCache &Cache = *GCache;    
   vector<string> matches;
   string res;

   found = false;
   for (pkgCache::PkgIterator Pkg = Cache.PkgBegin(); Pkg.end() == false; Pkg++)
   {
      pkgCache::VerIterator Ver = Pkg.VersionList();
      if (Ver.end())
         continue;
      for (pkgCache::PrvIterator Prv = Ver.ProvidesList(); Prv.end() == false; Prv++)
      {
         //Check for concrete packages
         if (!strcmp(virtualPkgName, Pkg.Name()))
         {
            cerr  << virtualPkgName << " is a concrete pkg!" << endl;
            return string();
         }

         if (strcmp(Prv.Name(), virtualPkgName) == 0)
         {
            if (res.length() > 0)
	      res += "\n";
	    res += string(Pkg.Name());
            
            found = true;
         }
      }

   }

   if (!found)
   {
      cerr << "  nothing provides <" << virtualPkgName << ">" << endl;
      return string();
   }

   return res;

}


/** TODO
static string PkgDesc(pkgCache* GCache, const char *PkgName, PyObject* version_list)
{

   for (pkgCache::PkgIterator Pkg = GCache->PkgBegin(); Pkg.end() == false; Pkg++)
   {
      int count_versions = 0;

      pkgCache::VerIterator Ver;
      if (strcmp(virtualPkgName, Pkg.Name()) != 0)
         continue;

      Ver = Pkg.VersionList();
      return Ver.Summary();

   }

   return "";


}
*/

static string
MatchPkgNames(pkgCache * GCache, const char *Pattern)
{

string matches;

 if(strlen(Pattern)==0)
    goto ret;

 for (pkgCache::PkgIterator P = GCache->PkgBegin(); P.end() == false; P++)
 {
         if (strstr(P.Name(),Pattern) != NULL && !P.VersionList().end())
         {
            if (matches.length() > 0)
                matches += "\n";
	        matches += string(P.Name());
         }
 }

ret: return matches;

}


DLL_PUBLIC const char* what_provides(const char *vpkgname)
{
	pkgCache* cache = openCache();
	string res = WhatProvidesDotty(cache, vpkgname);
	if (res.length() == 0)
		return NULL;
	else return res.c_str();
}

DLL_PUBLIC const char* requires(const char *pkgname)
{

	pkgCache* cache = openCache();
	string res = RequiresDotty(cache, pkgname);
	if (res.length() == 0)
		return NULL;
	else return res.c_str();
}

DLL_PUBLIC const char * pkg_search(const char *input)
{
	pkgCache* cache = openCache();
	string res = MatchPkgNames(cache, input);
	if (res.length() == 0)
		return NULL;
	else return res.c_str();

}
