#!/usr/bin/env python
import sys
sys.path.append('./build/lib.linux-x86_64-2.6/')
import pygraphviz as pgv
import aptdottypy as apt
import random

test_packages = ['bash', 'pidgin', 'glibc']

for p in test_packages:
    G = pgv.AGraph(directed='true')
    A = apt.dotty_pkg(p) #A=
    G.add_edges_from(A.edges)
    #print G.string()

    for ((o,d), attr) in zip(A.edges, A.edge_attrs):
        if attr:
            ed = G.get_edge(o, d)
            ed.attr['label'] = attr
    na = A.node_attrs            
    for pkgname in na.keys():
        node = G.get_node(pkgname)
        node.attr['label'] += na[pkgname]

    print G.string()
    #G.draw("/tmp/gumby-pgv-"+str(random.randint(1,10000)), prog='dot', format='xdot')




