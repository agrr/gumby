from ctypes import *
import re
import sys

'''
Here we will reimplement the existing API of aptdottypy.so
accessing the C functions through ctypes
There should be NO additional assumptions for the client code.
'''

aptpkg = CDLL('aptpkg-gumby.so')

class AptGraph:
    def __init__(self):
       self.edges = []
       self.node_attrs = {}
       self.edge_attrs = []

'''Returns an AptGraph object representing the first-level
dep tree of @pkgname '''
def dotty_pkg(pkgname):
    graph = AptGraph()
    if not pkgname or len(pkgname) < 3:
       raise ValueError, 'Invalid Package Name'
    #grab ctypes function
    requires = aptpkg.requires
    requires.restype = c_char_p
    s = requires(pkgname)
    print s +"\n"*3

    for i in s.split('\n'):
        op = ''
        ver = ''
        c = i.split(' ')
        if len(c) == 1:
            continue
        type = c[0]
        if len(c) == 2:
            req = c[1]
        elif len(c) == 4:
            req = c[3]
            op = c[1]
            ver = c[2]

        graph.edges.append((pkgname, req))
        if type != 'requires' or len(ver) > 0:
            graph.edge_attrs.append(' '.join([type, op, ver]))
        else:
            graph.edge_attrs.append('')
        #TODO: node_attrs

    return graph

def dotty_provides(pkgname):
    graph = AptGraph()
    provides = aptpkg.what_provides
    provides.restype = c_char_p
    s = provides(pkgname)

    for i in s.split('\n'):
        graph.edges.append((pkgname, i))

    return graph    

def dotty_pkgnames(input):
    pkg_search = aptpkg.pkg_search
    pkg_search.restype = c_char_p
    matching_pkgs = []
    for p in pkg_search(input).split('\n'):
        matching_pkgs.append(p)

    return matching_pkgs

print 'Testing dotty_pkgnames():'
print str(dotty_pkgnames('bash'))
print 'Testing dotty_provides():'
print str(dotty_provides('webclient').edges)
print str(dotty_provides('mail-server').edges)

"""args = sys.argv[1:] if len(sys.argv) > 1 else ['python']
for a in args:
    g=dotty_pkg(a)
    print str(g.edges), str(g.edge_attrs)
    """

'''
args = sys.argv[1:] if len(sys.argv) > 1 else ["python-devel"]
what_provides = libc.what_provides
what_provides.restype = c_char_p
requires = libc.requires
requires.restype = c_char_p

for a in args:
   res = what_provides(a)
   print res

pkgs = ['bash', 'python', 'firefox', 'perl', 'xterm']

for p in pkgs:
    print "%s deps: %s" % (p, requires(p))

'''    
