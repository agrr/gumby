#!/usr/bin/env python

import aptdottypy
import sys


def dedup(seq):
    seen = set()
    return [ x for x in seq if x not in seen and not seen.add(x)]

pkgs = ['coreutils', 'apt', 'emacs', 'qemulator', 'firefox']

#for p in pkgs:
#    print 'Dependencies for %s: %s' % (p, aptdottypy.gumby_dotty(p))
#    print '--------------------------------------'

if len(sys.argv) < 2:
    print 'Usage test-aptdotty PKGNAME'
    sys.exit(-1)

print aptdottypy.dotty_virtualpkg(sys.argv[1])

