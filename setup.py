from distutils.core import setup, Extension

libapt_includes=['/usr/include/apt-pkg', '/usr/include/rpm']
libapt=[('apt-pkg')]

dotty_module = Extension(
                'aptdottypy',
                 include_dirs = libapt_includes,
                 libraries = libapt,
                 language = 'c++',
                 sources = ['apt-dottyng.cc'])

version = '1.0'

setup(name = "gumby",
      version = version,
      author = 'Andre Guerreiro',
      author_email = 'andre.guerreiro@caixamagica.pt',
      description = 'Gumby is a dependency visualizer for apt-based systems',
      long_description = '''gumby is an interactive package dependency visualizing tool
for platforms where apt or apt-rpm is available.

It's implemented in Python using the GTK toolkit, Cairo vector graphics and Graphviz tools.
      ''',
      url = 'http://people.caixamagica.pt/aguerreiro/gumby',
      packages = ['gumby'],
      data_files = [('share/doc/gumby', ['README.rst', 'TODO']),
          ('share/applications/',['data/gumby.desktop']),
          ('share/pixmaps/',['data-devel/gumby.svg']),
          ('share/gumby/', ['gtk-builder/main-window.ui', 'gtk-builder/find-dialog.glade'])
            ],
      scripts = ['bin/gumby_launcher'],
      license = 'GPLv2',
      ext_modules = [dotty_module])

