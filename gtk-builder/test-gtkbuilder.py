#!/usr/bin/python
import gtk

def on_destroy(self):
            gtk.main_quit()

if __name__ == '__main__':
    builder = gtk.Builder()
    builder.add_from_file('teste.ui')
    window = builder.get_object('window1')
    window.set_title('FRom Code...')
    signal_dict = {
    'quit_menu_activated' : gtk.main_quit,
    'help_menu_activated': gtk.main_quit

    }
    #builder.connect_signals()

    window.show()
    window.connect("destroy", gtk.main_quit)
    
    gtk.main()

