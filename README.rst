Gumby Package Dependency Visualizer
===================================

gumby is an interactive package dependency visualizing tool
for platforms where apt or apt-rpm is available.

It's implemented in Python using the GTK toolkit and Graphviz tools for graph rendering.


Dependencies
===============================

 * python >= 2.5 (Not tested on older versions, it may work though)
 * pygtk >= 2.10
 * python-cairo
 * pygraphviz (which itself requires graphviz)
 * libapt-pkg (more suited to the apt-rpm version, it also works with the original apt)

Install 
==============================

 1. Make sure all the previously mentioned dependencies are installed
 2. Run ``python setup.py build`` in the gumby source directory to build the extension module
 3. To install globally run (as root) ``python setup.py install``

Credits
===============================
A lot of credit goes to xdot project (http://code.google.com/p/jrfonseca/wiki/XDot) for the
xdot Parser and Comix (http://comix.sourceforge.net/) for GTK tips and snippets.

License
===============================
Gumby is licensed under the GNU Public License Version 2: 
 http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
