/* Include Files							*{{{*/
//
#include "Python.h"
#include "structmember.h"
#include <apt-pkg/error.h>
#include <apt-pkg/init.h>
#include <apt-pkg/pkgcache.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/fileutl.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/version.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <vector>
#include <set>
#include <stdlib.h>
#include <sstream>
#include <unistd.h>
#include <errno.h>

using namespace std;

static FILE* random_source;

//Globals for storing a "persistent" connection to the apt cache


/*
 * Type Definition: apt-graph
 * class AptGraph:
 *   def __init__(self):
 *     self.edges = []
 *     self.node_attrs = {}
 *     self.edge_attrs = {}
 *
 *
 */

static pkgCache* openCache()
{
   pkgCache *GCache;

   if (pkgInitConfig(*_config) == false ||
       pkgInitSystem(*_config,_system) == false)
   {
      _error->DumpErrors();
      return NULL;
   }

   MMap *Map = new MMap(*new FileFd(_config->FindFile("Dir::Cache::pkgcache"),
	    FileFd::ReadOnly),MMap::Public|MMap::ReadOnly);

   if (_error->PendingError() == false)
   {
      GCache = new pkgCache(Map);

   }
   else
   {
      return NULL;
   }

   return GCache;
}

typedef struct {
   PyObject_HEAD
   PyObject * edges, *node_attrs, *edge_attrs;
}AptGraph;

static int
AptGraph_init(AptGraph *self)
{
   self->edges = PyList_New(0); // Tuples of package names (pkg,require)
   self->node_attrs = PyDict_New(); //N-ary Tuple for each package
   self->edge_attrs = PyList_New(0); //N-ary tuple for each relation
   return 0;
}

static int
AptGraph_dealloc(AptGraph *self)
{
   Py_XDECREF(self->edges);
   Py_XDECREF(self->node_attrs);
   Py_XDECREF(self->edge_attrs);
   self->ob_type->tp_free((PyObject*)self);
   return 0;
}

static PyObject*
AptGraph_repr(AptGraph *self)
{
   PyObject * edges_str = PyList_Type.tp_repr(self->edges);
   PyString_Concat(&edges_str, Py_BuildValue("s","\n"));
   PyString_Concat(&edges_str, PyList_Type.tp_repr(self->edge_attrs));
   PyString_Concat(&edges_str, PyDict_Type.tp_repr(self->node_attrs));
   return edges_str;
}


static int 
append_edge(AptGraph *self, const char *orig, const char *dest)
{

   if (!PyList_Append(self->edges,Py_BuildValue("(ss)",orig, dest))) {
      return NULL;
   }

   return 0;
}

static int 
append_node_attr(AptGraph *self, const char* nodeName, const char *attr)
{
   if (nodeName == NULL | attr == NULL)
   {
      cerr << "append_node_attr called for "<<nodeName << " without nodename or attr!"<<endl;
      return -1;
   }

   if (!PyDict_SetItem(self->node_attrs, Py_BuildValue("s",nodeName), Py_BuildValue("s", attr))) {
      return -1;
   }
   return 0;
}

static int 
append_edge_attr(AptGraph *self, const char *attr)
{
   //We need to match by list position, so insert 'None' objects
   if (attr == NULL)
   {
      PyList_Append(self->edge_attrs, Py_BuildValue(""));
      return 0;
   }

   if (!PyList_Append(self->edge_attrs, Py_BuildValue("s", attr))) {
      return -1;
   }
   return 0;
}



static PyMemberDef
AptGraph_members[] = {
   { "edges",   T_OBJECT, offsetof(AptGraph, edges), 0,
               "The list of edges as 2-tuples."},
               
   { "edge_attrs",  T_OBJECT, offsetof(AptGraph, edge_attrs), 0,
               "Dictionary of edge attributes."},

   { "node_attrs",  T_OBJECT, offsetof(AptGraph, node_attrs), 0,
               "List of node attributes."},
               
   { NULL }
};

static PyMethodDef
AptGraph_methods[] = {
   // typically there would be more here...
   
   { NULL }
};


static PyTypeObject
AptGraphType = {
   PyObject_HEAD_INIT(NULL)
   0,                         /* ob_size */
   "AptGraph",               /* tp_name */
   sizeof(AptGraph),         /* tp_basicsize */
   0,                         /* tp_itemsize */
   (destructor)AptGraph_dealloc, /* tp_dealloc */
   0,                         /* tp_print */
   0,                         /* tp_getattr */
   0,                         /* tp_setattr */
   0,                         /* tp_compare */
   (reprfunc)AptGraph_repr,                         /* tp_repr */
   0,                         /* tp_as_number */
   0,                         /* tp_as_sequence */
   0,                         /* tp_as_mapping */
   0,                         /* tp_hash */
   0,                         /* tp_call */
   0, //AptGraph_str,                         /* tp_str */
   0,                         /* tp_getattro */
   0,                         /* tp_setattro */
   0,                         /* tp_as_buffer */
   Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags*/
   "AptGraph",        /* tp_doc */
   0,                         /* tp_traverse */
   0,                         /* tp_clear */
   0,                         /* tp_richcompare */
   0,                         /* tp_weaklistoffset */
   0,                         /* tp_iter */
   0,                         /* tp_iternext */
   AptGraph_methods,         /* tp_methods */
   AptGraph_members,         /* tp_members */
   0,                         /* tp_getset */
   0,                         /* tp_base */
   0,                         /* tp_dict */
   0,                         /* tp_descr_get */
   0,                         /* tp_descr_set */
   0,                         /* tp_dictoffset */
   (initproc)AptGraph_init,  /* tp_init */
   0,                         /* tp_alloc */
   0,                         /* tp_new */
};




#define IDSIZE 12
/*
 * Generate a pseudo-random fixed-size string to use as 
 * node id, or any other kind of unique ID 
 *
 */

static string randomString()
{
   
   char random_string[IDSIZE+1];
   unsigned int i=0;

   while(i!= IDSIZE)
   {
      sprintf(&random_string[i],"%x",getc(random_source));
      i++;
   }

   random_string[IDSIZE] = '\0';

   return string(random_string);
}


static vector<const char *> * 
MatchPkgNames(pkgCache * GCache, const char *Pattern)
{

 vector<const char *> *matches = new vector<const char *>();

 if(strlen(Pattern)==0)
    goto ret;

 for (pkgCache::PkgIterator P = GCache->PkgBegin(); P.end() == false; P++)
 {
         if (strstr(P.Name(),Pattern) != NULL && !P.VersionList().end())
	    matches->push_back(P.Name());
 }

ret: return matches;

}

static bool IsSelfProvided(pkgCache * GCache, pkgCache::PkgIterator Pkg, 
      const char * Require)
{
      cerr << "Calling isSelfProvided for "<< Pkg.Name() << " and "<< Require; 
        for (pkgCache::PrvIterator I = Pkg.ProvidesList(); 
              I.end() == false; I++)
        {
	   if (strcmp(I.Name(), Require) == 0)
	   {
	      return true;
	    cerr << "... found it!" << endl;
	   }
	}

	cerr << endl;


	return false;

}

// RequiresDotty - Generate a dot document representing the Requires of pkgName	/*{{{*/
// ---------------------------------------------------------------------
static int RequiresDotty(pkgCache *GCache, const char *pkgName, AptGraph *requires_graph)
{
   pkgCache &Cache = *GCache;
   ostringstream out; 
   
   /* Initialize the list of packages to show.
      1 = To Show
      2 = To Show no recurse
      3 = Emitted no recurse
      4 = Emitted
      0 = None */
   enum States {None=0, ToShow, ToShowNR, DoneNR, Done};
   enum TheFlags {ForceNR=(1<<0)};
   unsigned char *Show = new unsigned char[Cache.Head().PackageCount];
   unsigned char *Flags = new unsigned char[Cache.Head().PackageCount];
   unsigned char *ShapeMap = new unsigned char[Cache.Head().PackageCount];
   
   for (unsigned long I = 0; I != Cache.Head().PackageCount; I++)
	 Show[I] = None;
   memset(Flags,0,sizeof(*Flags)*Cache.Head().PackageCount);
   
   // Map the shapes
   /*for (pkgCache::PkgIterator Pkg = Cache.PkgBegin(); Pkg.end() == false; Pkg++)
   {   
      if (Pkg->VersionList == 0)
      {
	 // Missing
	 if (Pkg->ProvidesList == 0)
	    ShapeMap[Pkg->ID] = 0;
	 else
	    ShapeMap[Pkg->ID] = 1;
      }
      else
      {
	 // Normal
	 if (Pkg->ProvidesList == 0)
	    ShapeMap[Pkg->ID] = 2;
	 else
	    ShapeMap[Pkg->ID] = 3;
      }
   }
   */
   
      // Process per-package flags
      string P = string(pkgName);
      
      // Locate the package
      pkgCache::PkgIterator Pkg = Cache.FindPkg(P);
      if (Pkg.end() == true)
      {
	 cerr << "Unable to locate package " << pkgName << endl;
	 return -1;
      }

      pkgCache::VerIterator Ver = Pkg.VersionList();
      //TODO: We're arbitrarily taking the first version of Pkg,
      //we have to expand them all

      for (pkgCache::DepIterator D = Ver.DependsList(); D.end() == false; D++)
      {
	 // See if anything can meet this dep
	 // Walk along the actual package providing versions
	 //(pkgCache::PkgIterator DPkg = D.TargetPkg();

	 // Only graph critical deps
	 // This is an inline function in libapt-pkg and its causing crashes 
	 // for no good reason, maybe re-implementing it here will solve ...	    
	 //if (D.IsCritical() == false)
	 //   continue;
	 //Dont include self-provided dependencies
	 if (D.TargetPkg() == 0)
	    continue;
	 
   	 if (IsSelfProvided(GCache, Pkg, D.TargetPkg().Name()))
	    continue;

	 bool notinstalled = false, virtual_pkg = false;

	 //Node attributes
	 if (D.TargetPkg().CurrentVer() == 0)
	    notinstalled = true;

	 if (ShapeMap[Pkg->ID] == 0 || ShapeMap[Pkg->ID] == 1)
	    virtual_pkg = true;

	 char *node_attr = NULL;
	 if (virtual_pkg)
	    node_attr = "virtual";
	 else if(notinstalled)
	    node_attr = "notinstalled";

	 append_node_attr(requires_graph, D.TargetPkg().Name(), node_attr);
	 append_edge(requires_graph, Pkg.Name(), D.TargetPkg().Name());

	 const int LABELSIZE=100;
	 char Label[LABELSIZE];
	 memset(Label, '\0', LABELSIZE);

	 char * default_label = NULL;
	 switch(D->Type)
	 {
	    case pkgCache::Dep::Conflicts:
	       sprintf(Label, "conflicts");
	       break;

	    case pkgCache::Dep::Obsoletes:    
	       sprintf(Label, "obsoletes");
	       break;
	 }
	 const char* comptype = D.CompType() != 0 ? D.CompType(): NULL;
	 const char* version = D.TargetVer() != 0 ? D.TargetVer(): NULL;
	 if (strlen(comptype) > 0)
	 {
	    strncat(Label, comptype, 90); 
	    strcat(Label, " "); 
	    strncat(Label, version, 90 - strlen(comptype));
	    default_label = Label;  
	 }

	 append_edge_attr(requires_graph, default_label);


      }
   

   return 0;
}

/**
 * This returns a dot graph of virtual package expansions 
 */

static int WhatProvidesDotty(pkgCache *GCache, AptGraph * provides, const char *virtualPkgName)
{

   bool found;
   ostringstream out;
   pkgCache &Cache = *GCache;    
   vector<string> matches;

   found = false;
   for (pkgCache::PkgIterator Pkg = Cache.PkgBegin(); Pkg.end() == false; Pkg++)
   {
      pkgCache::VerIterator Ver = Pkg.VersionList();
      if (Ver.end())
         continue;
      for (pkgCache::PrvIterator Prv = Ver.ProvidesList(); Prv.end() == false; Prv++)
      {
         //Check for concrete packages
         if (!strcmp(virtualPkgName, Pkg.Name()))
         {
            cerr  << virtualPkgName << " is a concrete pkg!" << endl;
            return -1;
         }

         if (!strcmp(Prv.Name(), virtualPkgName))
         {
            append_edge(provides, virtualPkgName, Pkg.Name());
            append_edge_attr(provides, "provided by");
	    append_node_attr(provides, virtualPkgName, "virtual");
            
            matches.push_back(Pkg.Name());
            found = true;
         }
      }

   }


   if (!found)
   {
      cerr << "  nothing provides <" << virtualPkgName << ">" << endl;
      return -1;
   }

   return 0;

}

/*
 * The strings returned here are not guaranteed to be proper node names for Graphviz!!
 * Some escaping or post-processing needs to be done at Python level...
 *
 */
static string VersionsOfDotty(pkgCache* GCache, const char *virtualPkgName, PyObject* version_list)
{
   ostringstream out;
   for (pkgCache::PkgIterator Pkg = GCache->PkgBegin(); Pkg.end() == false; Pkg++)
   {
      int count_versions = 0;

      pkgCache::VerIterator Ver;
      if (strcmp(virtualPkgName, Pkg.Name()) != 0)
         continue;

      Ver = Pkg.VersionList();

      for (; Ver.end() == false; Ver++) {
         //string version_id = randomString();

	 PyList_Append(version_list, Py_BuildValue("s", Ver.VerStr()));

      }

   }

   return out.str();
}


/** TODO
static string PkgDesc(pkgCache* GCache, const char *PkgName, PyObject* version_list)
{

   for (pkgCache::PkgIterator Pkg = GCache->PkgBegin(); Pkg.end() == false; Pkg++)
   {
      int count_versions = 0;

      pkgCache::VerIterator Ver;
      if (strcmp(virtualPkgName, Pkg.Name()) != 0)
         continue;

      Ver = Pkg.VersionList();
      return Ver.Summary();

   }

   return "";


}
*/

extern "C" PyObject*
dotty_pkg(PyObject *self, PyObject *args)
{
   const char * pkgname;
   AptGraph* graph = PyObject_New(AptGraph, &AptGraphType);
   AptGraph_init(graph); 
   if (!PyArg_ParseTuple(args, "s", &pkgname))
            return NULL;

   pkgCache *GCache = openCache();

   if(RequiresDotty(GCache, pkgname, graph) == -1)
	 return Py_BuildValue("");

   return (PyObject*)graph;
}


/* This method returns a Python list with all known packages
 * which name has the parameter as a substring */
extern "C" PyObject *
dotty_match_package_names(PyObject* self, PyObject *args)
{

   const char * pattern;
   if (!PyArg_ParseTuple(args, "s", &pattern))
      return NULL;

   pkgCache *GCache = openCache();

   PyObject *list;

   vector <const char *> *m = MatchPkgNames(GCache, pattern);
   list = PyList_New(0);

   for(int i = 0; i != m->size(); i++)
   {
      PyList_Append(list, Py_BuildValue("s", m->at(i)));

   }

  return list;
}



extern "C" PyObject *
dotty_virtualpkg(PyObject *self, PyObject *args)
{
   const char * pkgname;
   AptGraph* graph = PyObject_New(AptGraph, &AptGraphType);
   AptGraph_init(graph); 
   if (!PyArg_ParseTuple(args, "s", &pkgname))
            return NULL;
   
   pkgCache* cache = openCache();

   if(cache == NULL)
      cerr << "Failed to openCache at dotty_virtualpkg";

   if (WhatProvidesDotty(cache, graph, pkgname) == -1)
      return Py_BuildValue("");

   return (PyObject*)graph;

}

extern "C" PyObject *
dotty_versions(PyObject *self, PyObject *args)
{
   const char * pkgname;
   PyObject *result = PyList_New(0); 
   if (!PyArg_ParseTuple(args, "s", &pkgname))
            return NULL;
   
   pkgCache * Gcache = openCache();

   if (Gcache == NULL)
      cerr << "Failed to openCache at dotty_versions";

   VersionsOfDotty(Gcache, pkgname, result);

   return result;

}

/* TODO: Fix the first line of the docstrings to conform with PEP 7:
 * http://www.python.org/dev/peps/pep-0007/
 */

PyDoc_STRVAR(dottypkg__doc__, "Returns an instance of the AptGraph class describing the dependencies");
PyDoc_STRVAR(dottyvirtualkg__doc__, "Returns an instance of the AptGraph class containing the provides graph for the given virtual package");
PyDoc_STRVAR(dottyversions__doc__, "Returns a string containing the available versions graph for the given package");
PyDoc_STRVAR(dottymatchpackagenames__doc__, "Returns a list of pkgnames containing the available versions graph for the given package");
PyDoc_STRVAR(dottydescription__doc__, "Returns a string containing the short description for the given package");

/* registration table */
static PyMethodDef pyaptdotty_methods[] =
{
    {"dotty_pkg", dotty_pkg, METH_VARARGS, dottypkg__doc__},
    {"dotty_virtualpkg", dotty_virtualpkg, METH_VARARGS, dottyvirtualkg__doc__},
    {"dotty_versions", dotty_versions, METH_VARARGS, dottyversions__doc__},
    {"dotty_match_package_names", dotty_match_package_names, METH_VARARGS, dottymatchpackagenames__doc__},
   // {"dotty_description", dotty_description, METH_VARARGS, dottydescription__doc__},
    {NULL, NULL, 0, NULL}
};

static char * module_doc = "This module provides a small Python abstraction layer over "
"libapt-pkg which allows a number of queries to be done on apt\'s cache. "
"It defines a new type called AptGraph which stores a simple graph structure with optional node and edge string attributes.";

/* module initializer */
extern "C" void initaptdottypy()
{
   PyObject* mod;
   mod = Py_InitModule3("aptdottypy", pyaptdotty_methods, module_doc);
   random_source = (FILE*) fopen("/dev/urandom", "r");

   AptGraphType.tp_new = PyType_GenericNew;
   if (PyType_Ready(&AptGraphType) < 0) {
      return;
   }

   // Add the type to the module.
   Py_INCREF(&AptGraphType);
   PyModule_AddObject(mod, "AptGraph", (PyObject*)&AptGraphType);
}

