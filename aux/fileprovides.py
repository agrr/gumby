import os
import subprocess
import sys
import re

file_provides=open(sys.argv[1])

for file in file_provides:
    print file.strip() +" - ",
    #ret = os.system('rpm -qf %s' % file)
    #print "Using filename %s" % file
    p1 = subprocess.Popen(["rpm", "-qf", "%s" % (file.strip())], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = p1.stdout.read()
    errors = p1.stderr.read()
    if len(output) > 2:
        print output
    else:
        p2 = subprocess.Popen(["apt-cache", "whatprovides", "%s" % (file.strip())], stdout=subprocess.PIPE)
        print p2.stdout.read()


