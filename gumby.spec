%define pyver %(python -V 2>&1 | cut -f2 -d" " | cut -f1,2 -d".")

Name:           gumby
URL:            http://people.caixamagica.pt/aguerreiro/gumby
Summary:        Visual Dependency Browser
Version:        1.0
Release:        %mkrel 1
License:        GPLv2
Group:          System/Configuration/Packaging
Source:         %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-root

#We can't rely on RPM to autodetect Python library dependencies for now
Requires:	python-pygraphviz, pygtk2, python-cairo
BuildRequires:  libapt-pkg-devel
%py_requires -d

%description
Gumby is a GTK+ based application to browse your repository
package dependency graph.
It is built on top of libapt-pkg to allow the user to explore 
all the available packages and its inter-dependencies

%prep
%setup -q 

%build
%{__python} setup.py build

export PYTHONPATH="$RPM_BUILD_ROOT%{_libdir}/python%{pyver}/site-packages"

%install
%{__python} setup.py install --root=$RPM_BUILD_ROOT
#Don't ship any Python egg, please
rm $RPM_BUILD_ROOT/%{py_sitedir}/*.egg*

%clean
[ -d %{buildroot} -a "%{buildroot}" != "" ] && rm -rf  %{buildroot}

%files
%defattr(-,root,root)
%doc README.rst COPYING 
%{_bindir}/gumby_launcher
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/gumby.svg
%{py_sitedir}/gumby/*
%{py_sitedir}/aptdottypy.so


%changelog
* Wed Dec 06 2010 Andre Guerreiro <andre.guerreiro@caixamagica.pt> - 1.0-1xcm15
+ First Caixa Magica Package. Needs further testing on CM15

