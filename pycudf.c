#include <cudf.h>
#include <glib.h>
#include <stdlib.h>
#include <Python.h>


static PyObject* cudf_load(PyObject* self, PyObject *args)
{
   const char * cudf_path;
   PyObject *result = PyList_New(0); 
   if (!PyArg_ParseTuple(args, "s", &pkgname))
            return NULL;

	cudf_doc_t *doc = NULL;
	cudf_t *cudf = NULL, *sol = NULL;
	cudf_package_t pkg;
	cudf_universe_t univ = NULL;
	GList *l = NULL;
    	cudf_init();

}

PyMethodDef methods[] = {
        {"cudf_load", cudf_load, METH_VARARGS, "Returns a list of packages from a CUDF document as a list"},
            {NULL, NULL, 0, NULL}
};


PyMODINIT_FUNC 
initpycudf()
{
        (void) Py_InitModule("pycudf", methods);   
}

