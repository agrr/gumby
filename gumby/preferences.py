"""preferences.py - Preference handler."""

import os
import cPickle

import gtk
import pango

import constants
import labels

ZOOM_MODE_BEST = 0
ZOOM_MODE_WIDTH = 1
ZOOM_MODE_HEIGHT = 2
ZOOM_MODE_MANUAL = 3

# All the preferences are stored here.
prefs = {
    'orientation': 1,
    'aspect_ratio': 1.0,
    'font-size' : 2,
    'splines' : False,
    'show_tooltips': False,
    'layout_engine': 0,
    'node_fillcolor': (200, 200, 200),
    'edge_colour': (0, 0, 0),
    'window height': gtk.gdk.screen_get_default().get_height() * 3 // 4,
    'window width': min(gtk.gdk.screen_get_default().get_width() * 3 // 4,
                        gtk.gdk.screen_get_default().get_height() * 5 // 8),
}

_config_path = os.path.join(constants.get_data_directory(), 'preferences.pickle')
_dialog = None


class _PreferencesDialog(gtk.Dialog):
    
    """The preferences dialog where most (but not all) settings that are
    saved between sessions are presented to the user.
    """

    def __init__(self, window):
        self._window = window
        gtk.Dialog.__init__(self, 'Preferences', window, 0,
            (gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE))
        self.connect('response', self._response)
        self.set_has_separator(False)
        self.set_resizable(True)
        self.set_default_response(gtk.RESPONSE_CLOSE)
        notebook = gtk.Notebook()
        self.vbox.pack_start(notebook)
        self.set_border_width(4)
        notebook.set_border_width(6)
        
        # ----------------------------------------------------------------
        # The "Appearance" tab.
        # ----------------------------------------------------------------
        page = _PreferencePage(80)
        page.new_section('General Options')
        edge_color_label = gtk.Label('Edge color')
        color_button = gtk.ColorButton(gtk.gdk.Color(*prefs['edge_colour']))
        color_button.connect('color_set', self._color_button_cb, 'edge_colour')
        page.add_row(edge_color_label, color_button)
        
        node_color_label = gtk.Label('Node fillcolor')
        node_color_button = gtk.ColorButton(gtk.gdk.Color(*prefs['node_fillcolor']))
        node_color_button.connect('color_set', self._color_button_cb, 'node_fillcolor')
        page.add_row(node_color_label, node_color_button)
        engine_combo = gtk.combo_box_new_text()
        engine_combo.append_text('dot')
        engine_combo.append_text('neato')
        engine_combo.append_text('twopi')
        engine_combo.append_text('circo')
        # Change this if the combobox entries are reordered.
        engine_combo.set_active(prefs['layout_engine'])
        engine_combo.connect('changed', self._combo_box_cb,'layout_engine')
        page.add_row(gtk.Label('Graphviz Layout engine:'), engine_combo)

        double_page_button = gtk.CheckButton(
            'Show tooltips over the nodes.')
        double_page_button.set_active(prefs['show_tooltips'])
        double_page_button.connect('toggled', self._check_button_cb,
            'show_tooltips')
        page.add_row(double_page_button)
        label = gtk.Label('Orientation of layout')
        orientation_combo = gtk.combo_box_new_text()
        orientation_combo.append_text('Portrait')
        orientation_combo.append_text('Landscape')
        orientation_combo.set_active(prefs['orientation'])
        orientation_combo.connect('changed', self._combo_box_cb, 'orientation')
        page.add_row(label, orientation_combo)
        page.new_section('Layout Options')

        label = gtk.Label('Force Graph Aspect Ratio:')
        adjustment = gtk.Adjustment(prefs['aspect_ratio'],
            0, 3, 0.1, 1)
        aspect_spinner= gtk.SpinButton(adjustment, digits=2)
        aspect_spinner.connect('value_changed', self._spinner_cb,
            'aspect_ratio')
        page.add_row(label, aspect_spinner)
        
        double_page_button = gtk.CheckButton(
            'Use splines to draw edges')
        double_page_button.set_active(prefs['splines'])
        double_page_button.connect('toggled', self._check_button_cb,
            'splines')
        page.add_row(double_page_button)

        font_size_label = gtk.Label('Font Size:')
        fontsize_combo = gtk.combo_box_new_text()
        
        for n in range(8, 19):
            fontsize_combo.append_text(str(n))

        fontsize_combo.set_active(prefs['font-size'])
        page.add_row(font_size_label, fontsize_combo)
        
        notebook.append_page(page, gtk.Label('Options'))

        self.show_all()


    #TODO: Update the prefs names and specific callbacks
    def _check_button_cb(self, button, preference):
        """Callback for all checkbutton-type preferences."""
        prefs[preference] = button.get_active()
        if preference == 'splines':
            if not prefs[preference]:
                self._window.set_bg_colour(prefs['bg colour'])
            else:
                self._window.draw_image(scroll=False)

    def _color_button_cb(self, colorbutton, preference):
        """Callback for the background colour selection button."""
        colour = colorbutton.get_color()
        prefs['bg_colour'] = colour.red, colour.green, colour.blue

    def _spinner_cb(self, spinbutton, preference):
        #Special-case for integer values
        """Callback for spinner-type preferences."""
        value = spinbutton.get_value()
        prefs[preference] = value

    def _combo_box_cb(self, combobox, preference):
        """Callback for combobox-type preferences."""
        prefs[preference] = combobox.get_active()

    def _entry_cb(self, entry, event=None):
        """Callback for entry-type preferences."""
        text = entry.get_text()
        extensions = [e.strip() for e in text.split(',')]
        prefs['comment extensions'] = [e for e in extensions if e]
        self._window.file_handler.update_comment_extensions()

    def _response(self, dialog, response):
        _close_dialog()


class _PreferencePage(gtk.VBox):
    
    """The _PreferencePage is a conveniece class for making one "page"
    in a preferences-style dialog that contains one or more
    _PreferenceSections.
    """
    
    def __init__(self, right_column_width):
        """Create a new page where any possible right columns have the
        width request <right_column_width>.
        """
        gtk.VBox.__init__(self, False, 12)
        self.set_border_width(12)
        self._right_column_width = right_column_width
        self._section = None
    
    def new_section(self, header):
        """Start a new section in the page, with the header text from
        <header>.
        """
        self._section = _PreferenceSection(header, self._right_column_width)
        self.pack_start(self._section, False, False)

    def add_row(self, left_item, right_item=None):
        """Add a row to the page (in the latest section), containing one
        or two items. If the left item is a label it is automatically
        aligned properly.
        """
        if isinstance(left_item, gtk.Label):
            left_item.set_alignment(0, 0.5)
        if right_item is None:
            self._section.contentbox.pack_start(left_item)
        else:
            left_box, right_box = self._section.new_split_vboxes()
            left_box.pack_start(left_item)
            right_box.pack_start(right_item)


class _PreferenceSection(gtk.VBox):
    
    """The _PreferenceSection is a convenience class for making one
    "section" of a preference-style dialog, e.g. it has a bold header
    and a number of rows which are indented with respect to that header.
    """
    
    def __init__(self, header, right_column_width=150):
        """Contruct a new section with the header set to the text in
        <header>, and the width request of the (possible) right columns
        set to that of <right_column_width>.
        """
        gtk.VBox.__init__(self, False, 0)
        self._right_column_width = right_column_width
        self.contentbox = gtk.VBox(False, 6)
        label = labels.BoldLabel(header)
        label.set_alignment(0, 0.5)
        hbox = gtk.HBox(False, 0)
        hbox.pack_start(gtk.HBox(), False, False, 6)
        hbox.pack_start(self.contentbox)
        self.pack_start(label, False, False)
        self.pack_start(hbox, False, False, 6)

    def new_split_vboxes(self):
        """Return two new VBoxes that are automatically put in the section
        after the previously added items. The right one has a width request
        equal to the right_column_width value passed to the class contructor,
        in order to make it easy for  all "right column items" in a page to
        line up nicely.
        """
        left_box = gtk.VBox(False, 6)
        right_box = gtk.VBox(False, 6)
        right_box.set_size_request(self._right_column_width, -1)
        hbox = gtk.HBox(False, 12)
        hbox.pack_start(left_box)
        hbox.pack_start(right_box, False, False)
        self.contentbox.pack_start(hbox)
        return left_box, right_box


def open_dialog(action, window):
    global _dialog
    if _dialog is None:
        _dialog = _PreferencesDialog(window)
    else:
        _dialog.present()


def _close_dialog(*args):
    global _dialog
    if _dialog is not None:
        _dialog.destroy()
        _dialog = None


def read_preferences_file():
    """Read preferences data from disk."""
    if os.path.isfile(_config_path):
        config = None
        try:
            config = open(_config_path, 'rb')
            version = cPickle.load(config)
            old_prefs = cPickle.load(config)
            config.close()
        except Exception:
            print '! Corrupt preferences file "%s", deleting...' % _config_path
            if config is not None:
                config.close()
            os.remove(_config_path)
        else:
            for key in old_prefs:
                if key in prefs:
                    prefs[key] = old_prefs[key]


def write_preferences_file():
    """Write preference data to disk."""
    config = open(_config_path, 'wb')
    cPickle.dump(constants.VERSION, config, cPickle.HIGHEST_PROTOCOL)
    cPickle.dump(prefs, config, cPickle.HIGHEST_PROTOCOL)
    config.close()
