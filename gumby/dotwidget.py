#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2008 Jose Fonseca
# Copyright 2009-10 André Guerreiro
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''Visualize dot graphs via the xdot format'''

import os
import sys
import subprocess
import math
import time
import re

import gobject
import gtk
import gtk.gdk
import gtk.keysyms
import cairo

import preferences
import constants
import xdot_parser
from gumby_cairo import *
import aptdottypy
import filter as depfilter

import pygraphviz as pgv

# See http://www.graphviz.org/pub/scm/graphviz-cairo/plugin/cairo/gvrender_cairo.c

# For pygtk inspiration and guidance see:
# - http://mirageiv.berlios.de/
# - http://comix.sourceforge.net/





class Animation(object):

    step = 0.03 # seconds

    def __init__(self, dot_widget):
        self.dot_widget = dot_widget
        self.timeout_id = None

    def start(self):
        self.timeout_id = gobject.timeout_add(int(self.step * 1000), self.tick)

    def stop(self):
        self.dot_widget.animation = NoAnimation(self.dot_widget)
        if self.timeout_id is not None:
            gobject.source_remove(self.timeout_id)
            self.timeout_id = None

    def tick(self):
        self.stop()


class NoAnimation(Animation):

    def start(self):
        pass

    def stop(self):
        pass


class LinearAnimation(Animation):

    duration = 0.6

    def start(self):
        self.started = time.time()
        Animation.start(self)

    def tick(self):
        t = (time.time() - self.started) / self.duration
        self.animate(max(0, min(t, 1)))
        return (t < 1)

    def animate(self, t):
        pass


class MoveToAnimation(LinearAnimation):

    def __init__(self, dot_widget, target_x, target_y):
        Animation.__init__(self, dot_widget)
        self.source_x = dot_widget.x
        self.source_y = dot_widget.y
        self.target_x = target_x
        self.target_y = target_y

    def animate(self, t):
        sx, sy = self.source_x, self.source_y
        tx, ty = self.target_x, self.target_y
        self.dot_widget.x = tx * t + sx * (1-t)
        self.dot_widget.y = ty * t + sy * (1-t)
        self.dot_widget.queue_draw()


class ZoomToAnimation(MoveToAnimation):

    def __init__(self, dot_widget, target_x, target_y):
        MoveToAnimation.__init__(self, dot_widget, target_x, target_y)
        self.source_zoom = dot_widget.zoom_ratio
        self.target_zoom = self.source_zoom
        self.extra_zoom = 0

        middle_zoom = 0.5 * (self.source_zoom + self.target_zoom)

        distance = math.hypot(self.source_x - self.target_x,
                              self.source_y - self.target_y)
        rect = self.dot_widget.get_allocation()
        visible = min(rect.width, rect.height) / self.dot_widget.zoom_ratio
        visible *= 0.9
        if distance > 0:
            desired_middle_zoom = visible / distance
            self.extra_zoom = min(0, 4 * (desired_middle_zoom - middle_zoom))

    def animate(self, t):
        a, b, c = self.source_zoom, self.extra_zoom, self.target_zoom
        self.dot_widget.zoom_ratio = c*t + b*t*(1-t) + a*(1-t)
        self.dot_widget.zoom_to_fit_on_resize = False
        MoveToAnimation.animate(self, t)


class DragAction(object):

    def __init__(self, dot_widget):
        self.dot_widget = dot_widget

    def on_button_press(self, event):
        self.startmousex = self.prevmousex = event.x
        self.startmousey = self.prevmousey = event.y
        self.start()

    def on_motion_notify(self, event):
        if event.is_hint:
            x, y, state = event.window.get_pointer()
        else:
            x, y, state = event.x, event.y, event.state
        deltax = self.prevmousex - x
        deltay = self.prevmousey - y
        self.drag(deltax, deltay)
        self.prevmousex = x
        self.prevmousey = y

    def on_button_release(self, event):
        self.stopmousex = event.x
        self.stopmousey = event.y
        self.stop()

    def draw(self, cr):
        pass

    def start(self):
        pass

    def drag(self, deltax, deltay):
        pass

    def stop(self):
        pass

    def abort(self):
        pass


class NullAction(DragAction):

    def on_motion_notify(self, event):
        if event.is_hint:
            x, y, state = event.window.get_pointer()
        else:
            x, y, state = event.x, event.y, event.state
        dot_widget = self.dot_widget
        item = dot_widget.get_url(x, y)
        if item is None:
            item = dot_widget.get_jump(x, y)
        if item is not None:
            dot_widget.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.HAND2))
            dot_widget.set_highlight(item.highlight)
        else:
            dot_widget.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.ARROW))
            dot_widget.set_highlight(None)


class PanAction(DragAction):

    def start(self):
        self.dot_widget.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.FLEUR))

    def drag(self, deltax, deltay):
        self.dot_widget.x += deltax / self.dot_widget.zoom_ratio
        self.dot_widget.y += deltay / self.dot_widget.zoom_ratio
        self.dot_widget.queue_draw()

    def stop(self):
        self.dot_widget.window.set_cursor(gtk.gdk.Cursor(gtk.gdk.ARROW))

    abort = stop


class ZoomAction(DragAction):

    def drag(self, deltax, deltay):
        self.dot_widget.zoom_ratio *= 1.005 ** (deltax + deltay)
        self.dot_widget.zoom_to_fit_on_resize = False
        self.dot_widget.queue_draw()

    def stop(self):
        self.dot_widget.queue_draw()


class ZoomAreaAction(DragAction):

    def drag(self, deltax, deltay):
        self.dot_widget.queue_draw()

    def draw(self, cr):
        cr.save()
        cr.set_source_rgba(.5, .5, 1.0, 0.25)
        cr.rectangle(self.startmousex, self.startmousey,
                     self.prevmousex - self.startmousex,
                     self.prevmousey - self.startmousey)
        cr.fill()
        cr.set_source_rgba(.5, .5, 1.0, 1.0)
        cr.set_line_width(1)
        cr.rectangle(self.startmousex - .5, self.startmousey - .5,
                     self.prevmousex - self.startmousex + 1,
                     self.prevmousey - self.startmousey + 1)
        cr.stroke()
        cr.restore()

    def stop(self):
        x1, y1 = self.dot_widget.window2graph(self.startmousex,
                                              self.startmousey)
        x2, y2 = self.dot_widget.window2graph(self.stopmousex,
                                              self.stopmousey)
        self.dot_widget.zoom_to_area(x1, y1, x2, y2)

    def abort(self):
        self.dot_widget.queue_draw()

class RolloverButton(gtk.DrawingArea):
    pass


class DotWidget(gtk.DrawingArea):
    """PyGTK widget that draws dot graphs."""

    __gsignals__ = {
        'expose-event': 'override',
        'clicked': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, gtk.gdk.Event)),
        'right-clicked': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, gtk.gdk.Event)),
        'tooltip': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, gtk.gdk.Event))}

    filter = 'dot'

    def __init__(self, parentWindow, options):
        gtk.DrawingArea.__init__(self)
        print 'Initing DotWidget...'
        self.graph = Graph()
        self.opts = options
        self.compact_mode = False
        self.openfilename = None
        self.parent_class = parentWindow

        self.set_flags(gtk.CAN_FOCUS)
        self.set_size_request(824, 400) #Request a minimum height for the DrawingArea

        self.add_events(gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON_RELEASE_MASK)
        self.connect("button-press-event", self.on_area_button_press)
        self.connect("button-release-event", self.on_area_button_release)
        self.add_events(gtk.gdk.POINTER_MOTION_MASK | gtk.gdk.POINTER_MOTION_HINT_MASK | gtk.gdk.BUTTON_RELEASE_MASK)
        self.connect("motion-notify-event", self.on_area_motion_notify)
        self.connect("scroll-event", self.on_area_scroll_event)
        self.connect("size-allocate", self.on_area_size_allocate)
        self.connect('clicked', self.on_node_clicked)
        self.connect('right-clicked', self.on_node_clicked)
        self.connect('tooltip', self.draw_tooltip)

        self.connect('key-press-event', self.on_key_press_event)

        self.x, self.y = 0.0, 0.0
        self.zoom_ratio = 1.0
        self.zoom_to_fit_on_resize = False
        self.animation = NoAnimation(self)
        self.drag_action = NullAction(self)
        self.presstime = None
        self.highlight = None
        print 'Finished initing DotWidget...'

        
    #DEPRECATED: replaced  by xdot_sprintf
    '''Define suitable graphviz parameters for each specific tool'''
    def set_filter(self, filter):
        self.filter = filter
        self.options=[]
        if filter == 'neato':
            self.options.append('-Goverlap=scale')
            
        #self.options.append('-Txdot')

    def set_dotcode(self, dotcode, filename='<stdin>'):
        if isinstance(dotcode, unicode):
            dotcode = dotcode.encode('utf8')
        p = subprocess.Popen(
            [self.filter]+self.options,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=False,
            universal_newlines=True)
        
        xdotcode, error = p.communicate(dotcode)
        if p.returncode != 0:
            #Limit length error message from graphviz on the dialog
            full_error = error
            error = error[:177]+'...' if len(error) > 180 else error
            dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                                       message_format=error,
                                       buttons=gtk.BUTTONS_OK)
            sys.stderr.write(full_error)
            
            dialog.set_title('Dot Viewer')
            dialog.run()
            dialog.destroy()
            return False
        try:
            self.set_xdotcode(xdotcode)
        except ParseError, ex:
            dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                                       message_format=str(ex),
                                       buttons=gtk.BUTTONS_OK)
            dialog.set_title('Dot Viewer')
            dialog.run()
            dialog.destroy()
            return False
        else:
            self.openfilename = filename
            return True

    def set_xdotcode(self, xdotcode):
        parser = xdot_parser.XDotParser(xdotcode)
        self.graph = parser.parse()
        #self.graph.dumpShapes()
        #Debug print str(self.graph)

        self.zoom_image(self.zoom_ratio, center=True)

    def reload(self):
        if self.openfilename is not None:
            try:
                fp = file(self.openfilename, 'rt')
                self.set_dotcode(fp.read(), self.openfilename)
                fp.close()
            except IOError:
                pass

    def on_realize(self, widget):
        pass
    
    def on_size_allocate(self,widget, allocation):
        pass

    def do_expose_event(self, event):
        #cr = self.window.cairo_create()
        #self.cr = cr
        
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, event.area.width,
                event.area.height)
        #print 'Exposre event called with values (%d, %d)' % (event.area.width,
        #        event.area.height)
        self.surface = surface
        cr = cairo.Context(surface)

        # set a clip region for the expose event
        cr.rectangle(
            event.area.x, event.area.y,
            event.area.width, event.area.height
        )
        cr.clip()

        cr.set_source_rgba(1.0, 1.0, 1.0, 1.0)

        rect = self.get_allocation()
        cr.translate(0.5*rect.width, 0.5*rect.height)
        cr.scale(self.zoom_ratio, self.zoom_ratio)
        cr.translate(-self.x, -self.y)

        self.graph.draw(cr, highlight_items=self.highlight)

        self.drag_action.draw(cr)
        screen_context = self.window.cairo_create()
        screen_context.set_source_surface(surface)
        screen_context.paint()

        return True

    ##Get the area that's actually occupied by the drawing
    def get_active_area(self):
        outer_rectangle = None
        for s in self.graph.shapes:
            print 'shape found'
            #XXX:We should make sure this is really the outer bound
            if isinstance(s, PolygonShape):
                print 'Polygon shape found'
                outer_rectangle = s.points
                break

        if not outer_rectangle:
            print 'Something went wrong or maybe the graph is really empty'
            return None

        x2 = max([x for (x,y) in outer_rectangle])
        y2 = max([y for (x,y) in outer_rectangle])
        
        #We should only need max(x) and max(y), the origin point is always (self.x, self.y)
        return (x2, y2)

    def render_to_png(self, filename):
        #TODO: Crop to the relevant area using the tecnique of painting from one 
        #surface to another: http://cairographics.org/FAQ/#paint_from_a_surface
        screen_surface = self.surface
        (width, heigth) = self.get_active_area()
        cr.rectangle(
            self.x, self.y,
            width, height
        )
        cr.clip()
        #cr.paint()
        screen_surface.write_to_png(filename)

    def get_current_pos(self):
        return self.x, self.y

    def set_current_pos(self, x, y):
        self.x = x
        self.y = y
        self.queue_draw()

    def set_highlight(self, items):
        if self.highlight != items:
            self.highlight = items
            self.queue_draw()

    def zoom_image(self, zoom_ratio, center=False, pos=None):
        if center:
            self.x = self.graph.width/2
            self.y = self.graph.height/2
        elif pos is not None:
            rect = self.get_allocation()
            x, y = pos
            x -= 0.5*rect.width
            y -= 0.5*rect.height
            self.x += x / self.zoom_ratio - x / zoom_ratio
            self.y += y / self.zoom_ratio - y / zoom_ratio
        self.zoom_ratio = zoom_ratio
        self.zoom_to_fit_on_resize = False
        self.queue_draw()

    def zoom_to_area(self, x1, y1, x2, y2):
        rect = self.get_allocation()
        width = abs(x1 - x2)
        height = abs(y1 - y2)
        self.zoom_ratio = min(
            float(rect.width)/float(width),
            float(rect.height)/float(height)
        )
        self.zoom_to_fit_on_resize = False
        self.x = (x1 + x2) / 2
        self.y = (y1 + y2) / 2
        self.queue_draw()

    def zoom_to_fit(self):
        rect = self.get_allocation()
        rect.x += self.ZOOM_TO_FIT_MARGIN
        rect.y += self.ZOOM_TO_FIT_MARGIN
        rect.width -= 2 * self.ZOOM_TO_FIT_MARGIN
        rect.height -= 2 * self.ZOOM_TO_FIT_MARGIN
        zoom_ratio = min(
            float(rect.width)/float(self.graph.width),
            float(rect.height)/float(self.graph.height)
        )
        self.zoom_image(zoom_ratio, center=True)
        self.zoom_to_fit_on_resize = True

    ZOOM_INCREMENT = 1.25
    ZOOM_TO_FIT_MARGIN = 12

    def on_zoom_in(self, action):
        self.zoom_image(self.zoom_ratio * self.ZOOM_INCREMENT)

    def on_zoom_out(self, action):
        self.zoom_image(self.zoom_ratio / self.ZOOM_INCREMENT)

    def on_zoom_100(self, action):
        self.zoom_image(1.0)

    POS_INCREMENT = 100

    def on_key_press_event(self, widget, event):
        if event.keyval == gtk.keysyms.Left:
            self.x -= self.POS_INCREMENT/self.zoom_ratio
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Right:
            self.x += self.POS_INCREMENT/self.zoom_ratio
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Up:
            self.y -= self.POS_INCREMENT/self.zoom_ratio
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Down:
            self.y += self.POS_INCREMENT/self.zoom_ratio
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Page_Up:
            self.zoom_image(self.zoom_ratio * self.ZOOM_INCREMENT)
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Page_Down:
            self.zoom_image(self.zoom_ratio / self.ZOOM_INCREMENT)
            self.queue_draw()
            return True
        if event.keyval == gtk.keysyms.Escape:
            self.drag_action.abort()
            self.drag_action = NullAction(self)
            return True
        if event.keyval == gtk.keysyms.r:
            self.reload()
            return True
        return False

    #TODO: Deprecated
    def onSearch(self, widget):
        #pkgname = popupTextEntry('choose a package to display:')
        pkgname = popupGladeDialog('choose a package to display:', self.window)
        if len(pkgname) > 1:

            dot_code = aptdottypy.dotty_pkg(pkgName)
            if len(dot_code) == 0:
                dlg = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                message_format='The entered package was not found in apt database',
                                        buttons=gtk.BUTTONS_OK)
                dlg.set_title('Gumby')
                dlg.run()
                dlg.destroy()
                return
            else:
                popupFullErrorDialog("Can't find any matching package.\
                        \nPlease refine your query.")

    def xdot_sprintf(self, agraph):
        import tempfile
        (fd, name) = tempfile.mkstemp()
        options = '-Goverlap=scale' if self.filter == 'neato' else ''
        agraph.draw(name, format='xdot', prog=self.filter, args=options)
        content = open(name, 'r').read()
        os.remove(name)
        return content

    def edit_graph_attr(self, attr, value):
        self.current_graph.graph_attr.update(attr=value)

    def edit_edge_attr(self, attr, value):
        self.current_graph.node_attr.update(attr=value)

    def edit_node_attr(self, attr, value):
        self.current_graph.edge_attr.update(attr=value)

    def toggle_compact(self):
        if not self.compact_mode:
            for n in self.current_graph.nodes():
                n.attr['shape']='point'
            self.current_graph.node_attr.update(shape='point')
        else:
            for n in self.current_graph.nodes():
                n.attr['shape']='box'
            self.current_graph.node_attr.update(shape='box')
        self.set_xdotcode(self.xdot_sprintf(self.current_graph))
        self.compact_mode ^= True    

    #TODO: Eliminate constants, they should be config variables
    def display_package(self, pkgname='pidgin'):
        G = pgv.AGraph(landscape='true',directed='true')
        G.edge_attr.update(fontsize='10.0')
        initial_shape = 'box' if not self.compact_mode else 'point'
        G.node_attr['shape'] = initial_shape
        self.current_graph = G
        self.updateGraph(pkgname)

    def addNodeAttributes(self, graph, node_attrs):
        list = filter(lambda n: n not in depfilter.glibc_provides, node_attrs.keys())
        for n in list:
            g = graph.get_node(n)
            if 'notinstalled' == node_attrs[n]:
                g.attr['fillcolor'] = GREY
                g.attr['style'] = 'filled'
            #if 'virtual' == node_attrs[n]:
            #    g.attr['shape'] = 'ellipse'

    '''This is meant to be called after some change to user options'''
    def refresh_drawing(self):
        self.set_xdotcode(self.xdot_sprintf(G))
        
    
    '''Updates the pyGraphviz data structure, applying the configured attributes
    and any custom filtering'''
    def updateGraph(self, pkgname, fetch_func=aptdottypy.dotty_pkg):
        G = self.current_graph

        graph = fetch_func(pkgname)
        if not graph:
            return

        if self.opts.debug_mode:
            print graph

        i = 0
        j = 1
        for ((o,d), attr) in zip(graph.edges, graph.edge_attrs):
            if attr:
                if attr.find('conflicts') != -1:
                    G.add_edge(o, d, label=attr, color='red')
                elif attr.find('provided') != -1:
                    G.add_edge(o, d, label=attr, style='dashed')
                else:
                    G.add_edge(o, d, label=attr)

            else:
                G.add_edge(o, d)

        #Add the package name in the URL attr of the node
        #node = G.get_node(d)
        #node.attr['URL']= d
        #Do the same for the origin
        node = G.get_node(pkgname)
        node.attr['URL']= pkgname
        #FIXME: self.addNodeAttributes(G, graph.node_attrs)
        filt = depfilter.DepGraphFilter(G, pkgname)
        #filtered_graph = filt.filter_glibc()

        #G = filtered_graph
        print G.to_string()

        self.set_xdotcode(self.xdot_sprintf(G))

    #Node expansion/collapse logic
    #Different node types should yield different expansion types
    def on_node_clicked(self, widget, label, event):
        #FIXME: This is evil! Nice to simplify the drawing but breaks filtering etc.
        limit = self.parent_class.get_branching_factor()
        #Concrete package
        if len(aptdottypy.dotty_versions(label)) > 0:
            graph = aptdottypy.dotty_pkg(label)
            if self.opts.debug_mode:
                print graph
            for ((o,d), attr) in zip(graph.edges, graph.edge_attrs):
                if attr:
                    self.current_graph.add_edge(o, d, label=attr)
                else:
                    self.current_graph.add_edge(o, d)

            self.addNodeAttributes(self.current_graph, graph.node_attrs)
            filt = depfilter.DepGraphFilter(self.current_graph, label)
            filtered_graph = filt.filter_glibc()
            self.set_xdotcode(self.xdot_sprintf(filtered_graph))
        #Virtual Package
        else:
            self.updateGraph(label, aptdottypy.dotty_virtualpkg)

    def draw_tooltip(self, widget,text, event):
        tip = TooltipShape('hello world', 0, 10)
        tip.draw(self.cr)
        self.queue_draw()

    def get_drag_action(self, event):
        state = event.state
        if event.button in (1, 2): # left or middle button
            if state & gtk.gdk.CONTROL_MASK:
                return ZoomAction
            elif state & gtk.gdk.SHIFT_MASK:
                return ZoomAreaAction
            else:
                return PanAction
        return NullAction

    def on_area_button_press(self, area, event):
        self.animation.stop()
        self.drag_action.abort()
        action_type = self.get_drag_action(event)
        self.drag_action = action_type(self)
        self.drag_action.on_button_press(event)
        self.presstime = time.time()
        self.pressx = event.x
        self.pressy = event.y
        return False

    def is_click(self, event, click_fuzz=4, click_timeout=1.0):
        assert event.type == gtk.gdk.BUTTON_RELEASE
        if self.presstime is None:
            # got a button release without seeing the press?
            return False
        # XXX instead of doing this complicated logic, shouldn't we listen
        # for gtk's clicked event instead?
        deltax = self.pressx - event.x
        deltay = self.pressy - event.y
        return (time.time() < self.presstime + click_timeout
                and math.hypot(deltax, deltay) < click_fuzz)


    #TODO: Aqui e o handler dos cliques em nodes,
    #ver tb self.animate_to()
    def on_area_button_release(self, area, event):
        self.drag_action.on_button_release(event)
        self.drag_action = NullAction(self)
        x, y = int(event.x), int(event.y)
        label = self.get_label(x, y)
        if event.button == 1 and self.is_click(event):
            if label is not None:
                self.emit('clicked', label, event)
            #else:
                jump = self.get_jump(x, y)
                if jump is not None:
                    self.animate_to(jump.x, jump.y)

            return True
        if event.button == 2: #Handle middle-clicks
            #popupTextEntry('Middle-clicking on a node')
            return True
        if event.button == 3: #Handle right-clicks
            self.emit('right-clicked', label, event)
            return True
        return False

    def on_area_scroll_event(self, area, event):
        if event.direction == gtk.gdk.SCROLL_UP:
            self.zoom_image(self.zoom_ratio * self.ZOOM_INCREMENT,
                            pos=(event.x, event.y))
            return True
        if event.direction == gtk.gdk.SCROLL_DOWN:
            self.zoom_image(self.zoom_ratio / self.ZOOM_INCREMENT,
                            pos=(event.x, event.y))
            return True
        return False

    def on_area_motion_notify(self, area, event):
        self.drag_action.on_motion_notify(event)
        win = self.get_parent_window()
        x, y = event.x, event.y
        real_x, real_y = self.window2graph(x, y)
        self.graph.tooltip = (real_x, real_y)
        self.queue_draw()
        return True

    def on_area_size_allocate(self, area, allocation):
        if self.zoom_to_fit_on_resize:
            self.zoom_to_fit()

    def animate_to(self, x, y):
        self.animation = ZoomToAnimation(self, x, y)
        self.animation.start()

    def window2graph(self, x, y):
        rect = self.get_allocation()
        x -= 0.5*rect.width
        y -= 0.5*rect.height
        x /= self.zoom_ratio
        y /= self.zoom_ratio
        x += self.x
        y += self.y
        return x, y

    def get_url(self, x, y):
        x, y = self.window2graph(x, y)
        return self.graph.get_url(x, y)

    def get_label(self, x, y):
        x, y = self.window2graph(x, y)
        return self.graph.get_label(x, y)

    def get_jump(self, x, y):
        x, y = self.window2graph(x, y)
        return self.graph.get_jump(x, y)

class DotWindow:
    
    '''Deprecated'''
    def getScreenSize(self, window):
        scr = window.get_screen()
        return (scr.get_width(), scr.get_height())

    def on_find(self, widget):
        name = popupGladeDialog('choose a package to display:', self.window)
        if len(name) > 0:
            matches = aptdottypy.dotty_match_package_names(name)
            if len(matches) == 0:
                popupFullErrorDialog("Can't find any matching package.\nPlease refine your query.",
                        self.window)
                return False
            n = 0
            self.results_store.clear()
            for (n, item) in enumerate(matches):
                self.appendSearchResult([str(n+1), item])
            #context_id=self.statusbar.get_context_id('search')
            #self.statusbar.push(context_id, 'Found %d matching packages.' % n)

    def on_scaffolding(self, window):
        self.widget.display_package()

    def on_zoomin(self, widget):
        self.widget.on_zoom_in(widget)

    def on_zoomout(self, widget):
        self.widget.on_zoom_out(widget)
    
    def on_zoom100(self, widget):
        self.widget.on_zoom_100(widget)

    def on_fullscreen(self, widget):
        self.on_fullscreen(widget)

    def on_bestfit(self, widget):
        self.widget.zoom_to_fit()

    def appendSearchResult(self, item):
        self.results_store.append(item)

    def getSelectedPkg(self):
        selection = self.results_view.get_selection()
        model, iter = selection.get_selected()
        if iter:
            return model.get_value(iter, 1)

    def get_branching_factor(self):
        return self.branch_spinner.get_value_as_int()
        #return 12


    def __init__(self, options):
        print 'Building GUI...'

        self.graph = Graph()
        self.options = options
        self.fullscreen_state = False
        self.standalone_mode = True

        builder = gtk.Builder()
        #TODO at build-time get the datadir
        global gladefiles_path
        gladefiles_path = '/usr/local/share/gumby'
        builder.add_from_file(gladefiles_path+'/main-window.ui')
        window = builder.get_object('window1')
        self.results_store = builder.get_object('liststore1')

        #Setting up the TreeView 
        column_int = builder.get_object('column_id')
        column_str = builder.get_object('column_name')
        cell_int = builder.get_object('cellrenderertext1')
        scrollable = builder.get_object('scrolledwindow')
        cell_str = builder.get_object('cellrenderertext2')
        column_int.add_attribute(cell_int, 'text', 0)
        column_str.add_attribute(cell_str, 'text', 1) 
        scrollable.set_shadow_type(gtk.SHADOW_IN)
        treemodel_sort = builder.get_object('treemodelsort1')

        sortmodel = gtk.TreeModelSort(self.results_store)
        sortmodel.set_sort_column_id(1, gtk.SORT_ASCENDING)

        self.results_view= builder.get_object('treeview1')
        self.results_view.set_model(sortmodel)
        column_name= builder.get_object('column_name')
        column_name.set_sort_column_id(1)
        #self.statusbar = builder.get_object('statusbar')
        vbox1 = builder.get_object('vbox1')
        self.right_panel = builder.get_object('vbox_rightpanel')
        vbox_widget = builder.get_object('vbox_widget')
        self.toolbar = builder.get_object('toolbar')
        #xxx: These values should be loaded from .ui file, for some reason they dont
        adj = builder.get_object('adjustment1')
        adj.value = 4 
        adj.lower = 4
        adj.upper = 40
        self.branch_spinner = builder.get_object('spinbutton_branchlevel')

        self.widget = DotWidget(self, self.options)
        vbox_widget.pack_start(self.widget, expand=True)
        
        self.window = window
        builder.connect_signals(self)

        window.connect("destroy", gtk.main_quit)

        window.set_title('Gumby dependency viewer')
        icon_path = constants.loadIcon()
        logo = gtk.gdk.pixbuf_new_from_file(icon_path)
        window.set_icon(logo)

        window.set_focus(self.widget)
        print 'Finished Building GUI...'


    def not_implemented(self, event):
        print 'NOT IMPLEMENTED YET'
        return True
    
    '''NOTICE: only call once for the lifetime of the program'''
    def show_gui(self):
        self.window.maximize()
        self.window.show_all()
        gtk.main()

    def on_help(self, event):
        import glib
        glib.spawn_async(['/usr/bin/xdg-open', 'http://people.caixamagica.pt/aguerreiro/gumby/'])
    
    #TODO: to be completed
    def apply_changes(self, event):
        pass

    def applySynapticMode(self):
        #Hide search panel
        self.right_panel.hide()
        #Hide search button
        self.toolbar.get_nth_item(5).hide()
        #TODO Hide search function from menu

    def change_graph_filter(self, action, current):
        current_filter = ('dot', 'neato', 'twopi', 'circo')[action.get_current_value()]
        self.widget.set_filter(current_filter)

    #TODO: This function is saving the whole drawing area 
    #TODO: Implement cropping to the relevant area
    #TODO; The filechooser should open in $HOME by default and save the choice made by the user
    def on_save_as_image(self, event):
        #Open Filename Chooser
        filename = None
        chooser=gtk.FileChooserDialog('Save your graph as...', None, gtk.FILE_CHOOSER_ACTION_SAVE,
                (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                    gtk.STOCK_SAVE, gtk.RESPONSE_ACCEPT))
        chooser.set_default_response(gtk.RESPONSE_ACCEPT)
        chooser.set_current_name('graph.png')
        if chooser.run() == gtk.RESPONSE_ACCEPT:
            filename=chooser.get_filename()
            print 'Saving graph to: '+filename
        
        chooser.destroy()

        #Render the graph to a PNG File
        if filename:
            self.widget.render_to_png(filename)

    def on_about(self, event):
        about_box = about.GumbyDialog()
        about_box.show()

    def on_load_package(self, event):
        selected_pkg = self.getSelectedPkg()
        if not selected_pkg:
            return False
        #context_id=self.statusbar.get_context_id('search')
        #self.statusbar.push(context_id, 'Loading package depgraph...')
        self.widget.display_package(selected_pkg)
        #self.widget.set_dotcode(dot_code)

    def on_toggle_radio(self, widget, data=None):
        state = widget.get_active()
        print 'toggle state: '+str(state)
        
        if state == 1:
            print "Changing GV filter to %s" % data 
            self.set_filter(data) 

    def on_clear(self, event):
        self.results_store.clear()

    def quit(self, event):
        gtk.main_quit()

    def set_filter(self, filter):
        self.widget.set_filter(filter)

    def loadInitialPackage(self, pkgname):
        self.widget.display_package(pkgname) #We need to store it for later merging
        #self.widget.set_dotcode(dot_code)

    def set_dotcode(self, dotcode, filename='<stdin>'):
        if self.widget.set_dotcode(dotcode, filename):
            #self.set_title(os.path.basename(filename) + ' - Dot Viewer')
            self.widget.zoom_to_fit()

    def set_xdotcode(self, xdotcode, filename='<stdin>'):
        if self.widget.set_xdotcode(xdotcode):
            #self.set_title(os.path.basename(filename) + ' - Dot Viewer')
            self.widget.zoom_to_fit()

    def open_file(self, filename):
        try:
            fp = file(filename, 'rt')
            self.set_dotcode(fp.read(), filename)
            fp.close()
        except IOError, ex:
            dlg = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                                    message_format=str(ex),
                                    buttons=gtk.BUTTONS_OK)
            dlg.set_title('Dot Viewer')
            dlg.run()
            dlg.destroy()

    def on_preferences(self, action):
        preferences.open_dialog(action, self.window)

    def on_open(self, action):
        chooser = gtk.FileChooserDialog(title="Open dot File",
                                        action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                        buttons=(gtk.STOCK_CANCEL,
                                                 gtk.RESPONSE_CANCEL,
                                                 gtk.STOCK_OPEN,
                                                 gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        filter = gtk.FileFilter()
        filter.set_name("Graphviz dot files")
        filter.add_pattern("*.dot")
        chooser.add_filter(filter)
        filter = gtk.FileFilter()
        filter.set_name("All files")
        filter.add_pattern("*")
        chooser.add_filter(filter)
        if chooser.run() == gtk.RESPONSE_OK:
            filename = chooser.get_filename()
            chooser.destroy()
            self.open_file(filename)
        else:
            chooser.destroy()

    def on_fullscreen(self, action):
        window = self.window
        if self.fullscreen_state:
            window.unfullscreen()
        else:
            window.fullscreen()
        self.fullscreen_state = not self.fullscreen_state    

    def on_reload(self, action):
        self.widget.reload()

    def on_compact_mode(self, action):
        self.widget.toggle_compact()

'''DEPRECATED'''
def popupTextEntry(text):
        #base this on a message dialog
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_QUESTION,
            gtk.BUTTONS_OK,
            None)
        dialog.set_markup(text)
        #create the text input field
        entry = gtk.Entry()
        #allow the user to press enter to do ok
        entry.connect("activate", lambda self, widget, response: dialog.response(response), dialog, gtk.RESPONSE_OK)
        #create a horizontal box to pack the entry and a label
        hbox = gtk.HBox()
        #hbox.pack_start(gtk.Label("Package Name (must be exact):"), False, 5, 5)
        hbox.pack_end(entry)
        #add it and show it
        dialog.vbox.pack_end(hbox, True, True, 0)
        dialog.show_all()
        #go go go
        dialog.run()
        text = entry.get_text()
        dialog.destroy()
        return text

def popupFullErrorDialog(text, parent_window):
    dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
            message_format=text,
            buttons=gtk.BUTTONS_OK)
    sys.stderr.write(text)

    dialog.set_title('Gumby')
    dialog.run()
    dialog.destroy()
    return False

def popupGladeDialog(text, parent_window):
    builder = gtk.Builder()
    builder.add_from_file(gladefiles_path+'/find-dialog.glade')
    dialog = builder.get_object('gumby-findpkg')
    dialog.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    dialog.set_transient_for(parent_window)
    ok = builder.get_object('ok_button')
    cancel = builder.get_object('cancel_button')
    entry = builder.get_object('entry1')
    ok.connect('clicked', dialog.response)
    #cancel.connect('clicked', dialog.destroy)

    dialog.run()
    text = entry.get_text()
    dialog.destroy()
    return text

