# -*- coding: utf-8 -*-
"""about.py - About dialog."""

'''
AboutDialog class

'''


import os
import sys
import gtk

import constants

class GumbyDialog:

    def __init__(self):
        dialog = gtk.AboutDialog()
        dialog.connect('response', self.destroy)
        dialog.set_website('http://gitorious.org/cairo-graphs/gumby/')
        dialog.set_website_label('Gitorious repository')
        dialog.set_license('''This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.''')
        dialog.set_name('Gumby')
        dialog.set_version(constants.VERSION)
        dialog.set_authors( [u'André Guerreiro', u'José Fonseca - Xdot Project', 
            u'André Ricardo - Artwork'])
        dialog.set_comments('Gumby is a package dependency viewer for APT-based systems')
        dialog.set_copyright(u'Copyright \u00a9 2009-2010 André Guerreiro')
        icon_path = constants.loadIcon()
        logo = gtk.gdk.pixbuf_new_from_file(icon_path)
        dialog.set_logo(logo)
        gtk.about_dialog_set_url_hook(self.openWebpage, dialog.get_website_label)
        self.dialog = dialog

    def destroy(self, *args):
        if self.dialog:
            self.dialog.destroy()
    
    def openWebpage(self, link, userdata):
        os.system("xdg-open %s" % link)

    def show(self):        
        self.dialog.show()



