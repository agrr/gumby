#!/usr/bin/env python
# - coding: utf-8 -
# Copyright (C) 2010 Toms Bauģis <toms.baugis at gmail.com>

"""
 Games with points, implemented following Dr. Mike's Maths
 http://www.dr-mikes-maths.com/
"""


import gtk
from lib import graphics

import math
from lib.euclid import Vector2, Point2
import itertools
import random


EPSILON = 0.00001
YELLOW = (255, 255, 0)
STRONG_YELLOW = (218, 165, 32)
WHITE = (255, 255, 255)
BLACK = (0,0,0)

class CrossButton(graphics.Sprite):
    def __init__(self, x, y, parent):
        self.x = x 
        self.y = y
        graphics.Sprite.__init__(self, x = x, y = y, interactive = True)
        self.connect('on-click', self.add_button)
        cr.set_line_style(5)
        cr.move_to(10,-20)
        cr.rel_line_to(0, 20)
        cr.move_to(0, -10)
        cr.rel_line_to(20, 0)
        cr.stroke()

class Node(graphics.Sprite):
    def __init__(self, x, y, text, fontsize=10):
        self.x = x
        self.y = y
        graphics.Sprite.__init__(self, x = x, y = y, interactive = True, draggable = True)
        '''self.add_child(graphics.Label(text, fontsize, '#000'))
        self.graphics.rectangle(x, y, 30, 30, 3)
        self.graphics.stroke('#000')
        '''
        self.text = text
        self.button_visible = False
        
        self.connect("on-render", self.on_render)
        self.connect('on-click', self.add_button)

    def add_button(self, sprite, b):
        pass

    def on_render(self, sprite):
        cr = self.graphics
        #cr.select_font_face("Sans Serif")
        #cr.set_font_size(11)
        #Yellow background, with stronger yellow outline

        cr.set_color(BLACK)
        cr.rectangle(-20 , -20, 40, 40, 0)
        cr.stroke()
        #cr.rectangle(5, 5, 10, 10, 0)
        #cr.stroke()

        cr.move_to(self.x-18, self.y-8) #Very hackish, we should use Pango hints to draw bounding boxes
        cr.show_text(self.text)

class Scene(graphics.Scene):
    def __init__(self):
        graphics.Scene.__init__(self)
        self.nodes = []
        self.nodes.append(Node(100, 200,'bash'))
        self.nodes.append(Node(100, 250, 'glibc'))
        self.nodes.append(Node(200, 300, 'libtermcap'))
        self.nodes.append(Node(399, 235, 'ldconfig'))
        self.nodes.append(Node(399, 235, 'locale'))
        self.nodes.append(Node(399, 235, 'dash-static'))
        self.adj_list = [(0,1), (0,2), (1,3), (1,4), (1,5)]
        for i in self.nodes:
            self.add_child(i)

        self.connect("on-enter-frame", self.on_enter_frame)
        self.connect("on-drag", self.on_node_drag)

        self.draw_circles = False

    def on_node_drag(self, scene, node, coords):
        #print "Nodes: "+str(self.nodes)
        self.redraw()


    def on_enter_frame(self, scene, context):
        #print "Nodes: "+str(self.nodes)
        # voronoi diagram
        context.set_source_rgb(0.7, 0.7, 0.7)
        #segments = list(self.voronoi())
        #for node, node2 in segments:
        #    context.move_to(node.x, node.y)
        #    context.line_to(node2.x, node2.y)
        for a, b in self.adj_list:
          n1 = self.nodes[a]
          n2 = self.nodes[b]
          context.move_to(n1.x, n1.y)
          context.line_to(n2.x, n2.y)
        
        context.stroke()



class BasicWindow:
    def __init__(self):
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_size_request(600, 500)
        window.connect("delete_event", lambda *args: gtk.main_quit())

        self.scene = Scene()

        box = gtk.VBox()
        box.pack_start(self.scene)


        window.add(box)
        window.show_all()


if __name__ == "__main__":
    example = BasicWindow()
    gtk.main()
