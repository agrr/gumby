# -*- coding: utf-8 -*-
AUTHOR = "Andre Guerreiro"

VERSION = "0.95"
datadir = '/usr/share/gumby'
ICON_PATH=['data-devel/gumby.svg', '/usr/share/pixmaps/gumby.svg', '/usr/share/gumby/pixmaps/gumby.svg']

import os
__MAIN_ICON__= None


def get_home_directory():
    """On UNIX-like systems, this method will return the path of the home
    directory, e.g. /home/username. 
    """
    return os.path.expanduser('~')

def get_data_directory():
    base_path = os.getenv('XDG_DATA_HOME',
       os.path.join(get_home_directory(), '.local/share'))
    return os.path.join(base_path, 'gumby')

def loadIcon():
    global __MAIN_ICON__
    if __MAIN_ICON__:
        return __MAIN_ICON__
    for path in ICON_PATH:
        try:
            if os.stat(path):
                __MAIN_ICON__= path
                return path
        except OSError:
            continue
