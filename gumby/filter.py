glibc_provides = ['libc.so', 'libm.so', 'rtld(GNU_HASH)', 'libdl.so', 'libm.so', 'ld-linux.so', 
           'libpthread.so', 'ld-linux', 'librt.so']

libstdcpp_provides = ['libstdc++.so']

libgcc_provides = ['libgcc_s']

class DepGraphFilter:
   
   #TODO: it should be in a config file or retrieved at startup from the system glibc package

   def __init__(self, edges, root_pkg=None, debug=False):
       self.graph = edges
       self.debug_mode = debug
       self.root = root_pkg

   def __matchList__(self, string):
        for i in glibc_provides:
            if string.find(i) != -1:
                return True  
        return False

   def filter_glibc(self):
       glibc_found = False
       edges_to_remove = []
       #we're assuming to be filtering a tree with just one parent 
       origin = self.root
       for (orig, dest) in self.graph.edges_iter():
           #We shouldn't refilter the previous edges
           if orig != origin:
               continue
           if self.__matchList__(dest):
               edges_to_remove.append((orig,dest))
        
       for n in edges_to_remove:
           if self.debug_mode:
               print "Edge to remove "+ str(n)
           orig, dest = n
           self.graph.remove_edge(orig, dest)
           self.graph.remove_node(dest)

       if len(edges_to_remove) > 0:
           self.graph.add_node('glibc', URL='glibc')
           self.graph.add_edge(origin, 'glibc')

       return self.graph

